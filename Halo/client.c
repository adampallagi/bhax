/***************************************/
/*             TCP client              */
/***************************************/
 
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
 
#define BUFFER_SIZE 1024
#define PORT_NO 2001
#define error(a,b) fprintf(stderr, a, b)

struct card
{
   char* color;
   int value;
   int id;
};


void deck_init(struct card deck[])
{
for(int i=0;i<52;i++)
   {
      if(i<13)
      {
         deck[i].color = "Diamonds";
      }
      else if(i<26)
      {
         deck[i].color = "Clubs";
      }
      else if(i<39)
      {
         deck[i].color = "Hearts";
      }
      else
      {
         deck[i].color = "Spades";
      }

      deck[i].value = (i % 13) + 2;

      deck[i].id = i;

   }

   //11 = jacks
   //12 = queen
   //13 = king
   //14 = ace
}
 
int main(int argc, char *argv[]) 
{   
 
   /* Declarations */
   int fd;                         // socket endpoint
   int flags;                      // receive flags
   struct sockaddr_in server;      // socket name (addr) of server 
   struct sockaddr_in client;      // socket name of client 
   int server_size;                // length of the socket addr. server 
   int client_size;                // length of the socket addr. client 
   int bytes;                      // length of buffer 
   int rcvsize;                    // received bytes
   int trnmsize;                   // transmitted bytes
   int err;                        // error code
   int ip;                         // ip address
   char on;                        // 
   char buffer[BUFFER_SIZE+1];         // datagram dat buffer area
   char server_addr[16];           // server address    
   char* null = "";

   //Game stuff

   struct card deck[52];
   deck_init(deck);

   struct card p_deck[5];
   char* p_buffer = malloc(BUFFER_SIZE);
   char win_msg[BUFFER_SIZE +1];
   int boolean = 0;
   int boolean_1 = 0;
   int game_start = 1;
   int shutdown_msg = 0;
   char* money_msg = malloc(BUFFER_SIZE+1);
   char* game_continue = malloc(BUFFER_SIZE+1);



   /* Initialization */
   on    = 1;
   flags = 0;
   server_size = sizeof server;
   client_size = sizeof client;
   sprintf(server_addr, "%s", argv[1]);
   ip = inet_addr(server_addr);
   server.sin_family      = AF_INET;
   server.sin_addr.s_addr = ip;
   server.sin_port        = htons(PORT_NO);

   // Creating socket
   fd = socket(AF_INET, SOCK_STREAM, 0);
   if (fd < 0) {
      error("%s: Socket creation error.\n",argv[0]);
      exit(1);
   }

   // Setting socket options
   setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on);
   setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, (char *)&on, sizeof on);

   // Connecting to the server
   err = connect(fd, (struct sockaddr *) &server, server_size);
   if (err < 0) 
   {
      error("%s: Cannot connect to the server.\n", argv[0]);
      exit(2);
   }

   //------------------------------------------------------------------------------------

   recv(fd, buffer, BUFFER_SIZE, 0);
   printf("%s\n", buffer);

   printf("Please type 'y', if you are ready!\n");
   while(boolean == 0)
   {
      scanf("%s", buffer);
      if(buffer[0] == 'y')
      {
         send(fd, buffer, BUFFER_SIZE, 0);
         boolean = 1;
      }
      else 
      {
         printf("Please enter y or quit\n");
         boolean = 0;
      }
   }

   while(shutdown_msg == 0)
   {
      printf("New round commencing!\n");

      recv(fd, p_buffer, BUFFER_SIZE, 0);

      const char s[2] = ",";
      char *token;

      token = strtok(p_buffer, s);
      for(int i=0; i<5 && token != NULL; i++) 
      {
        p_deck[i].id = atoi(token);
        token = strtok(NULL, s);
      }

      printf("-------------------------------------\n");
      printf("Your Cards Are:\n\n");

      for(int i=0;i<5;i++)
      {
         for(int j=0;j<52;j++)
         {
            if(p_deck[i].id == deck[j].id)
            {
               p_deck[i].color = deck[j].color;
               p_deck[i].value = deck[j].value;
            }
         }
         printf("%s of %d\n", p_deck[i].color, p_deck[i].value );
      }
      printf("\n");

      char temp[5];
      char msg[5] = {'n', 'n', 'n', 'n', 'n'};
      for(int i=0;i<5;i++)
      {
         printf("Would you like to swap card #%d?\t(y or n)\n", i+1);
         scanf(" %c", &temp[i]);
         if(temp[i] == 'y')
         {
            msg[i] = temp[i];
         }
         else if(temp[i] != 'n')
         {
            printf("Invalid input!\n");
            --i;
         }
      }

      send(fd, msg, BUFFER_SIZE, 0);

      recv(fd, p_buffer, BUFFER_SIZE, 0);

      token = strtok(p_buffer, s);
      for(int i=0; i<5 && token != NULL; i++) 
      {
        p_deck[i].id = atoi(token);
        token = strtok(NULL, s);
      }

      printf("-------------------------------------\n");
      printf("\nYour new cards Are:\n\n");

      for(int i=0;i<5;i++)
      {
         for(int j=0;j<52;j++)
         {
            if(p_deck[i].id == deck[j].id)
            {
               p_deck[i].color = deck[j].color;
               p_deck[i].value = deck[j].value;
            }
         }
         printf("%s of %d\n", p_deck[i].color, p_deck[i].value );
      }

      recv(fd, win_msg, BUFFER_SIZE, 0);
      recv(fd, money_msg, BUFFER_SIZE, 0);

      printf("\n-------------------------------------\n%s\n", win_msg);
      printf("%s\n", money_msg);

      printf("Do you want to continue playing? (y or n)\n");
      
      game_continue[0] = 'p';
      scanf("%s", game_continue);
      if((game_continue[0] != 'y') && (game_continue[0] != 'n'))
      {
         printf("Invalid input!\n");
         scanf("%s", game_continue);
      }

      if(game_continue[0] == 'n')
      {
         send(fd, game_continue, BUFFER_SIZE, 0);
         boolean_1 = 1;
         printf("Exiting, the other player wins!\n");
      }
      else if(game_continue[0] == 'y')
      {
         send(fd, game_continue, BUFFER_SIZE, 0);
         boolean_1 = 1;
         printf("Continuing game!\n");
      }

      recv(fd, game_continue, BUFFER_SIZE, 0);

      if(game_continue[0] == 'n')
      {
         close(fd);
         printf("A player has disconnected, the game has ended.\n");
         printf("\n%s\n", money_msg);
         break;
      }


   }

   return 0;
} 