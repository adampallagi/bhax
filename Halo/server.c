#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h> 

#define BUFFER_SIZE 1024                  	    // buffer size
#define PORT_NO 2001                      		// port number
#define error(a,b) fprintf(stderr, a, b)  		// error 'function'

struct card
{
	char* color;
	int value;
	int id;
};

char* createString(struct card p_deck[])
{	
	char* msg = malloc(BUFFER_SIZE);
	char* id_str;
	for(int i=0;i<5;i++)
	{
		sprintf(id_str, "%d", p_deck[i].id);
		strcat(msg, id_str);
		strcat(msg, ",");
	}
	return msg;
}

void swap(struct card* deck, int i, struct card* p_deck, int j)
{
    struct card temp;
    temp = deck[i];
    deck[i] = p_deck[j];
    p_deck[i] = temp;
}

int max(int a, int b)
{
	if(a>=b)
	{
		return a;
	}
	else
	{
		return b;
	}
}

void deck_init(struct card deck[])
{
for(int i=0;i<52;i++)
	{
		if(i<13)
		{
			deck[i].color = "Diamonds";
		}
		else if(i<26)
		{
			deck[i].color = "Clubs";
		}
		else if(i<39)
		{
			deck[i].color = "Hearts";
		}
		else
		{
			deck[i].color = "Spades";
		}

		deck[i].value = (i % 13) + 2;

		deck[i].id = i;

	}

	//11 = jacks
	//12 = queen
	//13 = king
	//14 = ace
}

void shuffle(struct card deck[])
{
    int size = 52;
    for (int i=0;i<size;i++)
    {
	    int j = rand() % 52;
	    struct card temp = deck[j];
	    deck[j] = deck[i];
	    deck[i] = temp;
    }
}

void p1_deal(struct card deck[], struct card p1_deck[])
{
	for(int i=0;i<5;i++)
	{
		p1_deck[i] = deck[i];
	}
}

void p2_deal(struct card deck[], struct card p2_deck[])
{
	for(int i=5;i<10;i++)
	{
		p2_deck[i-5] = deck[i];
	}
}

int check_straight_flush(struct card p_deck[])
{
	struct card temp;
	int flush = 0;
	int straight = 0;
	char* color;

	color = p_deck[0].color;
	if((color == p_deck[1].color) && (color == p_deck[2].color) && (color == p_deck[3].color) && (color == p_deck[4].color))
	{
		flush = 1;
	}

	for(int i=0;i<4;i++)
	{
		if(p_deck[i].value > p_deck[i+1].value)
		{
			temp = p_deck[i];
			p_deck[i] = p_deck[i+1];
			p_deck[i+1] = temp;
		}
	}
	if(p_deck[0].value == p_deck[1].value + 1 == p_deck[2].value + 2 == p_deck[3].value + 3 == p_deck[4].value + 4)
	{
		straight = 1;
	}
	if(flush && straight)
	{
		return p_deck[4].value;
	}
	
}

int check_four_of_a_kind(struct card p_deck[])
{
	for(int i=0;i<5;i++)
	{
		for(int j=0;j<5;j++)
		{
			for(int k=0;k<5;k++)
			{
				for(int l=0;l<5;l++)
				{
					if((p_deck[i].value == p_deck[j].value == p_deck[k].value == p_deck[l].value) && (p_deck[i].color != p_deck[j].color) && (p_deck[j].color != p_deck[k].color && (p_deck[k].color != p_deck[l].color) && (p_deck[l].color != p_deck[i].color)))
					{
						return p_deck[i].value;
					}
				}
			}
		}
	}
	return 0;
}

int check_full_house(struct card p_deck[])
{
	int full_house_value = 0;
	int pair_value = 0;
	for(int i=0;i<5;i++)
	{
		for(int j=0;j<5;j++)
		{
			for(int k=0;k<5;k++)
			{
				if((p_deck[i].value == p_deck[j].value == p_deck[k].value) && (p_deck[i].color != p_deck[j].color) && (p_deck[j].color != p_deck[k].color && (p_deck[k].color != p_deck[i].color)))
				{
					full_house_value = 1;
				}
			}
		}
	}

	for(int i=0;i<5;i++)
	{
		for(int j=0;j<5;j++)
		{
			if((p_deck[i].value == p_deck[j].value) && (p_deck[i].color != p_deck[j].color) && p_deck[i].value != full_house_value)
			{
				pair_value =  p_deck[i].value;
			}
		}
	}
	return full_house_value;

}

int check_flush(struct card p_deck[])
{
	char* color = p_deck[0].color;
	if((color == p_deck[1].color) && (color == p_deck[2].color) && (color == p_deck[3].color) && (color == p_deck[4].color))
	{
		return 5;
	}
	else
	{
		return 0;
	}
}

int check_straight(struct card p_deck[])
{
	struct card temp;

	for(int i=0;i<4;i++)
	{
		if(p_deck[i].value > p_deck[i+1].value)
		{
			temp = p_deck[i];
			p_deck[i] = p_deck[i+1];
			p_deck[i+1] = temp;
		}
	}
	if(p_deck[0].value == p_deck[1].value + 1 == p_deck[2].value + 2 == p_deck[3].value + 3 == p_deck[4].value + 4)
	{
		return p_deck[4].value;
	}
	else
	{
		return 0;
	}
}

int check_three_of_a_kind(struct card p_deck[])
{
	for(int i=0;i<5;i++)
	{
		for(int j=0;j<5;j++)
		{
			for(int k=0;k<5;k++)
			{
				if((p_deck[i].value == p_deck[j].value == p_deck[k].value) && (p_deck[i].color != p_deck[j].color) && (p_deck[j].color != p_deck[k].color && (p_deck[k].color != p_deck[i].color)))
				{
					return p_deck[i].value;
				}
			}
		}
	}
	return 0;
}

int check_two_pairs(struct card p_deck[])
{
	for(int i=0;i<5;i++)
	{
		for(int j=0;j<5;j++)
		{
			if((p_deck[i].value == p_deck[j].value) && (p_deck[i].color != p_deck[j].color))
			{
			for(int k=0;k<5;k++)
				{
					for(int l=0;l<5;l++)
					{
						if((p_deck[k].value == p_deck[l].value) && (p_deck[k].color != p_deck[l].color) && (p_deck[k].value != p_deck[i].value))
						{
							return max(p_deck[i].value, p_deck[k].value);
						}
					}
				}
			}
		}
	}
	return 0;
}

//utoljára kell vizsgálni
int check_pair(struct card p_deck[])
{
	for(int i=0;i<5;i++)
	{
		for(int j=0;j<5;j++)
		{
			if((p_deck[i].value == p_deck[j].value) && (p_deck[i].color != p_deck[j].color))
			{
				return (p_deck[i].value);
			}
		}
	}
	return 0;

}

int check_high_card(struct card p_deck[])
{
	return max(max(max(p_deck[0].value, p_deck[1].value), max(p_deck[2].value, p_deck[3].value)), p_deck[4].value);
}



int main(int argc, char* argv[])
{

	//server stuff

	/* Declarations */
	int fd;                       		   // socket endpoint
	struct sockaddr_in server;			// socket name (addr) of server
	struct sockaddr_in client_1;		// socket name of client 1
	struct sockaddr_in client_2;    	// socket name of client 2
	int server_size;                 // length of the socket addr. server
	int client_1_size;				// length of the socket addr. client 1
	int client_2_size;               // length of the socket addr. client 2
	int bytes;                       // length of buffer 
	int rcvsize;                     // received bytes
	int trnmsize;                    // transmitted bytes
	int err;                         // error code
	char on;                         
	char buffer[BUFFER_SIZE+1];
	char buffer1[BUFFER_SIZE+1];
	char buffer2[BUFFER_SIZE+1];        
	int player1_socket;
	int player2_socket;

	//Game stuff

	int max_money = atoi(argv[1]);
	char* start_message;
	int game_start;
	char* p1_buffer;
	char* p2_buffer;
	char p1_swap_msg[5];
	char p2_swap_msg[5];
	int win_msg = 0;
	int p1_money = max_money/2;
	int p2_money = max_money/2;
	int bank_money = 0;
	char* msg = malloc(BUFFER_SIZE +1);
	char* p1_money_str = malloc(BUFFER_SIZE+1);
	char* p2_money_str = malloc(BUFFER_SIZE+1);
	char* money_msg = malloc(BUFFER_SIZE+1);
	char* p1_game_continue = malloc(BUFFER_SIZE+1);
	char* p2_game_continue = malloc(BUFFER_SIZE+1);

	/* Initialization */
	on = 1;
	bytes = BUFFER_SIZE;
	server_size = sizeof server;
	client_1_size = sizeof client_1;
	client_2_size = sizeof client_2;
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(PORT_NO);

      /* Creating socket */
   fd = socket(AF_INET, SOCK_STREAM, 0 );
   if (fd < 0) {
      error("%s: Socket creation error\n",argv[0]);
      exit(1);
   }
 
   /* Setting socket options */
   setsockopt(player1_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on);
   setsockopt(player2_socket, SOL_SOCKET, SO_REUSEADDR, (char *)&on, sizeof on);
 
   /* Binding socket */
   err = bind(fd, (struct sockaddr *) &server, server_size);
   if (err < 0) {
      error("%s: Cannot bind to the socket\n",argv[0]);
      exit(2);
      }
 
   /* Listening */
   err = listen(fd, 10);
   if (err < 0) {
      error("%s: Cannot listen to the socket\n",argv[0]);
      exit(3);
      }

    //accept
	player1_socket = accept(fd, (struct sockaddr *) &client_1, &client_1_size);
	if (fd < 0) 
	{
		error("%s: Cannot accept player 1 on socket\n",argv[0]);
		exit(4);
    }
    else
    {
    	printf("Player 1 has joined\n");
    }

	player2_socket = accept(fd, (struct sockaddr *) &client_2, &client_2_size);
	if (fd < 0) 
	{
		error("%s: Cannot accept player 2 on socket\n",argv[0]);
		exit(4);
    }
    else
    {
    	printf("Player 2 has joined\n");
    }


	start_message = "Starting Game\n";

	memcpy(buffer, start_message, strlen(start_message)+1);
	send(player1_socket, buffer, BUFFER_SIZE, 0);
	send(player2_socket, buffer, BUFFER_SIZE, 0);

	recv(player1_socket, buffer1, BUFFER_SIZE, 0);
	recv(player2_socket, buffer2, BUFFER_SIZE, 0);

	if(buffer1[0] == 'y' && buffer2[0] == 'y')
	{
		game_start = 1;
	}

	struct card deck[52];
	struct card p1_deck[5];
	struct card p2_deck[5];
	int p1_points = 0;
	int p2_points = 0;

	while(1)
	{
		p1_money = p1_money - 50;
		p2_money = p2_money - 50;
		bank_money = bank_money + 100;

		srand(time(NULL));

		deck_init(deck);
		shuffle(deck);

		p1_deal(deck, p1_deck);
		p2_deal(deck, p2_deck);

		printf("Cards of Player 1:\n");
		for(int i=0;i<5;i++)
		{
			printf("%s of %d\n", p1_deck[i].color, p1_deck[i].value );
		}

		printf("--------------------------------\n");

		printf("Cards of Player 2:\n");
		for(int i=0;i<5;i++)
		{
			printf("%s of %d\n", p2_deck[i].color, p2_deck[i].value );
		}

		p1_buffer = createString(p1_deck);
		send(player1_socket, p1_buffer, BUFFER_SIZE, 0);
		p2_buffer = createString(p2_deck);
		send(player2_socket, p2_buffer, BUFFER_SIZE, 0);

		recv(player1_socket, p1_swap_msg, BUFFER_SIZE, 0);
		recv(player2_socket, p2_swap_msg, BUFFER_SIZE, 0);

		for(int i=0;i<5;i++)
		{
			if(p1_swap_msg[i] == 'y')
			{
				p1_deck[i] = deck[i+10];
			}
		}
		for(int i=0;i<5;i++)
		{
			if(p2_swap_msg[i] == 'y')
			{
				p2_deck[i] = deck[i+15];
			}
		}

		p1_buffer = createString(p1_deck);
		send(player1_socket, p1_buffer, BUFFER_SIZE, 0);
		p2_buffer = createString(p2_deck);
		send(player2_socket, p2_buffer, BUFFER_SIZE, 0);

		printf("--------------------------------\n");

		printf("Cards of Player 1:\n");
		for(int i=0;i<5;i++)
		{
			printf("%s of %d\n", p1_deck[i].color, p1_deck[i].value );
		}

		printf("--------------------------------\n");

		printf("Cards of Player 2:\n");
		for(int i=0;i<5;i++)
		{
			printf("%s of %d\n", p2_deck[i].color, p2_deck[i].value );
		}



		if(((p1_points == 0)) && (p2_points == 0))
		{
			p1_points = check_straight_flush(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p2_points = check_straight_flush(p2_deck);
		}
		if(((p1_points == 0)) && (p2_points == 0))
		{
			p1_points = check_four_of_a_kind(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p2_points = check_four_of_a_kind(p2_deck);
		}
		if(((p1_points == 0)) && (p2_points == 0))
		{
			p1_points = check_full_house(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p2_points = check_full_house(p2_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p1_points = check_flush(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p2_points = check_flush(p2_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p1_points = check_straight(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p2_points = check_straight(p2_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p1_points = check_three_of_a_kind(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p2_points = check_three_of_a_kind(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p1_points = check_two_pairs(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p2_points = check_two_pairs(p2_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p1_points = check_pair(p1_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p2_points = check_pair(p2_deck);
		}
		if((p1_points == 0) && (p2_points == 0))
		{
			p1_points = check_high_card(p1_deck);
			p2_points = check_high_card(p2_deck);
		}

		printf("--------------------------------\n");

		printf("p1: %d\tp2: %d\n", p1_points, p2_points);

		if(p1_points > p2_points)
		{
			msg = "Player 1 has won!";
			printf("%s\n", msg);
			send(player1_socket, msg, BUFFER_SIZE, 0);
			send(player2_socket, msg, BUFFER_SIZE, 0);
			p1_money += bank_money;
		}
		else if(p2_points > p1_points)
		{
			msg = "Player 2 has won!";
			printf("%s\n", msg);
			send(player1_socket, msg, BUFFER_SIZE, 0);
			send(player2_socket, msg, BUFFER_SIZE, 0);
			p2_money += bank_money;
		}
		else
		{
			msg = "It's a Draw!";
			printf("%s\n", msg);
			send(player1_socket, msg, BUFFER_SIZE, 0);
			send(player2_socket, msg, BUFFER_SIZE, 0);
			p1_money += bank_money/2;
			p2_money += bank_money/2;
		}

		bank_money = 0;

		printf("--------------------------------\n");

		memset(money_msg, 0, sizeof(money_msg));

		sprintf(p1_money_str, "%d", p1_money);
		sprintf(p2_money_str, "%d", p2_money);
		strcat(money_msg, "Player 1 money: ");
		strcat(money_msg, p1_money_str);
		strcat(money_msg, ", Player 2 money: ");
		strcat(money_msg, p2_money_str);

		send(player1_socket, money_msg, BUFFER_SIZE, 0);
		send(player2_socket, money_msg, BUFFER_SIZE, 0);

		if((p1_money > 0) && (p2_money > 0))
		{
			game_start = 1;
		}
		else
		{
			game_start = 0;
		}

		recv(player1_socket, p1_game_continue, BUFFER_SIZE, 0);
		recv(player2_socket, p2_game_continue, BUFFER_SIZE, 0);

		if((p1_game_continue[0] == 'n') && (p2_game_continue[0] == 'n'))
		{
			printf("Both players quit, the game has ended.\n");

			p1_game_continue[0] = 'n';
			p2_game_continue[0] = 'n';
			send(player1_socket, p1_game_continue, BUFFER_SIZE, 0);
			send(player2_socket, p2_game_continue, BUFFER_SIZE, 0);
			close(player1_socket);
			close(player2_socket);

			break;
		}
		else if((p1_game_continue[0] == 'y') && (p2_game_continue[0] == 'n'))
		{
			printf("Player 2 has quit, Player 1 won!\n");
		
			p1_game_continue[0] = 'n';
			p2_game_continue[0] = 'n';
			send(player1_socket, p1_game_continue, BUFFER_SIZE, 0);
			send(player2_socket, p2_game_continue, BUFFER_SIZE, 0);
			printf("%s\n", p1_game_continue);
			printf("%s\n", p2_game_continue);
			close(player1_socket);
			close(player2_socket);

			break;

		}
		else if((p1_game_continue[0] == 'n') && (p2_game_continue[0] == 'y'))
		{
			printf("Player 1 has quit, Player 2 won!\n");	

			p1_game_continue[0] = 'n';
			p2_game_continue[0] = 'n';
			send(player1_socket, p1_game_continue, BUFFER_SIZE, 0);
			send(player2_socket, p2_game_continue, BUFFER_SIZE, 0);
			printf("%s\n", p1_game_continue);
			printf("%s\n", p2_game_continue);
			close(player1_socket);
			close(player2_socket);
			break;
		}
		else
		{
			p1_game_continue[0] = 'y';
			p2_game_continue[0] = 'y';
			send(player1_socket, p1_game_continue, BUFFER_SIZE, 0);
			send(player2_socket, p2_game_continue, BUFFER_SIZE, 0);
		}

	}
	return 0;
}